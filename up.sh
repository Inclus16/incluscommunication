#!/usr/bin/env bash

docker-compose down
docker network create inclus
docker network create inclus_db
docker-compose build node
rm -R docker/nginx/front/
docker-compose run --rm node npm run build
mkdir docker/nginx/front/
cp -R incluscommunicationclient/dist/ docker/nginx/front/dist/
docker-compose build db
docker-compose up -d db
#docker-compose build net
docker-compose build nginx
docker-compose up -d net nginx
